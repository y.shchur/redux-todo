import {ADD_TODO} from './actionTypes';
let nextTodoId = 0
export function addTodo(text) {
  return {
    id: nextTodoId++,
    type: ADD_TODO,
    text
  }
}
