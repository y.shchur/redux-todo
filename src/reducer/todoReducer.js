const initialState = {
  todos: {
    allIds: [],
    byIds: {

    }
  }
};

const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      const newState = {
        ...state,
        ...{todos: groupTodoById(state.todos, action)}
      };
      console.log(newState);
      return newState;
    // case 'TOGGLE_TODO':
    //   return state.map(todo =>
    //     (todo.id === action.id)
    //       ? {...todo, completed: !todo.completed}
    //       : todo
    //   );
    default:
      return state
  }
};

const groupTodoById = (todos, action) => {
  return {
    allIds: [...todos.allIds, action.id],
    byIds: {
      ...todos.byIds,
      [action.id]: {
        text: action.text,
        completed: false
      }
    }
  }
}

export default todoReducer;
