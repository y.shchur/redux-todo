import React from 'react';
import {connect} from 'react-redux';

class TodoList extends React.Component {

  render() {
    return (
      <ul>{this.props.todos.map(todo => <li key={todo.id}>{todo.text}</li>)}</ul>
    );
  }
}

const mapStateToProps = state => {
  const {byIds, allIds} = state.todos || {};
  console.log(state);
  const todos =
    allIds && allIds.length
      ? allIds.map(id => (byIds ? {...byIds[id], id} : null))
      : [];
  return {todos};
};


export default connect(mapStateToProps)(TodoList);
