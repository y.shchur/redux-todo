import React from 'react';
import { addTodo } from '../action/actionCreators';
import { connect } from 'react-redux';

class AddTodo extends React.Component {


  constructor(props, context) {
    super(props, context);
    this.state = { input: '' };
  }

  updateInput = (text) => {
    this.setState({ input: text })
  };

  handleAddTodo = () => {
    this.props.addTodo(this.state.input);
    this.setState({ input: '' });
  };

  render() {
    return <div>
      <input
        value={this.state.input}
        onChange={e => this.updateInput(e.target.value)}
      />
      <button className="add-todo" onClick={this.handleAddTodo}>
        Add Todo
      </button>
    </div>
  }
}

export default connect(
  null,
  { addTodo }
)(AddTodo);
